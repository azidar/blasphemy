# README #

This README normal documents whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Building Blasphemy! cards
* Version 0.0.1

### How do I get set up? ###

* Need to install ruby, I used brew.
* Then install squib (see http://www.squib.rocks). I used gem to install squib, but it couldn't install dependencies properly, so whenever the installation of squib would crash, I would install the missing dependency using brew.
* Then, build everything
```
ruby blasphemy.rb
```
* You can also build only parts of decks
```
ruby blasphemy.rb followers
ruby blasphemy.rb elements
ruby blasphemy.rb actions
```

### Who do I talk to? ###

* azidar
* Any Izzy brother

### To Do ###

- Upload pngs to website
