require 'squib'
require 'ostruct'

def svgPath(symbol) 
  "svg/" + symbol + ".svg"
end

def buildFollowers(back, n)
  deckFile = 'csv/' + n.to_s + 'fronts.csv'
  backgroundColor = 'black'
  textColor = 'white'
  side = ''
  prefix = n.to_s + 'follower_'
  file = n.to_s + 'front.pdf'
  title = 'SKEPTIC'
  rotate = 'clockwise'
  if back
    side = 'back'
    deckFile = 'csv/' + n.to_s + 'backs.csv'
    backgroundColor = 'white'
    textColor = 'black'
    prefix = n.to_s + 'follower_'
    file = n.to_s + 'back.pdf'
    title = 'FOLLOWER'
    rotate = 'counterclockwise'
  end
  ncards = 0
  if n == 3
    ncards = 21
  end
  if n == 2
    ncards = 28
  end
  if n == 1
    ncards = 35
  end
  # 3 Follower Fronts
  # 21 cards

  Squib::Deck.new(cards: ncards, layout: 'yml/minicard.yml', width: 825, height: 600) do
    deck = csv file: deckFile
    names = csv file: 'names/3followerNames.csv'
    empires = (csv file: 'names/' + n.to_s + 'followerEmpires.csv')['Empires']
    rect layout: 'MiniCard', stroke_color: textColor, fill_color: :black
    rect layout: 'MiniBleed', stroke_color: textColor, fill_color: backgroundColor
  
    if n == 1
      text str: empires.flat_map{|x| [x] * 5}, layout: :FollowerTitle, color: textColor
    else
      text str: empires.flat_map{|x| [x] * n}, layout: :FollowerTitle, color: textColor
    end
    text str: title, layout: :LargeType, color: textColor
    #text str: names['Names'][0..21], layout: :LeftName, color: textColor
    #text str: names['Names'][21..42], layout: :MiddleName, color: textColor
    #text str: names['Names'][42..63], layout: :RightName, color: textColor
  
    if(n == 3)
      svg file: deck['Left'].map{|x| svgPath(x)}, layout: :Left3Color
    end
    if(n == 2)
      svg file: deck['Left'].map{|x| svgPath(x)}, layout: :Left2Color
    end
    if(n == 3 or n == 1)
      svg file: deck['Middle'].map{|x| svgPath(x)}, layout: :MiddleColor
    end
    if(n == 2)
      svg file: deck['Right'].map{|x| svgPath(x)}, layout: :Right2Color
    end
    if(n == 3)
      svg file: deck['Right'].map{|x| svgPath(x)}, layout: :Right3Color
    end
  
    #save_png prefix: '3front_'
    if ARGV.include?("png")
      if back
        save_png rotate: :counterclockwise, count_format: '%02d' + side, dir: '_output/followers', prefix: prefix
      else
        save_png rotate: :clockwise, count_format: '%02d' + side, dir: '_output/followers', prefix: prefix
      end
    else
      #save_pdf file: '3front.pdf', trim: 41.25, margin: 120
      save_pdf file: file, margin: 120
    end
  end
end

png = ARGV.include?("png")

if ARGV.size == 0 or ARGV.include?("followers")
  buildFollowers(false, 3)
  buildFollowers(true, 3)
  buildFollowers(false, 2)
  buildFollowers(true, 2)
  buildFollowers(false, 1)
  buildFollowers(true, 1)
end

def makeRadial(insideColor, outsideColor)
    '(412.5,300,100)(412.5,300,500) ' + insideColor + '@0.0 ' + outsideColor + '@1.0'
end

if ARGV.size == 0 or ARGV.include?("elements") or ARGV.include?("aspects")
  # Aspect Cards
  # 24 per Aspect
  # Total: 168
  Squib::Deck.new(cards: 168, layout: 'yml/minicard.yml', width: 825, height: 600) do
    background color: 'white'
    deck = csv file: 'csv/elements.csv'
    colors = deck['Aspect'].map{ |x|
      ret = OpenStruct.new
      case x
      when "Scripture"
        makeRadial("DARK_GREEN", "LIGHT_GREEN")
      when "Holiday"
        makeRadial("PERSIAN_INDIGO", "LAVENDER")
      when "Custom"
        makeRadial("DARK_GOLDENROD", "CREAM")
      when "Metaphysics"
        makeRadial("BLACK", "DARK_GRAY")
      when "Liturgy"
        makeRadial("NAVY_BLUE", "SKY_BLUE")
      when "Divinity"
        #makeRadial("GAINSBORO", "white")
        makeRadial("GRAY", "white")
      when "Taboo"
        makeRadial("BISTRE", "CRIMSON")
      end
    }

    rect layout: 'MiniCard', fill_color: :black
    rect layout: 'MiniBleed', fill_color: colors
    rect layout: 'Text', fill_color: :beige, stroke_color: :black

    rect layout: 'RTitle'
    text str: deck['Title'], layout: :RTitle, valign: :middle
    text str: deck['Text'], layout: :Text, valign: :middle
    svg file: deck['Aspect'].map{|x| svgPath(x)}, layout: :AspectColor
    rect layout: 'Type'
    text str: 'Element', layout: :Type
  
    #save_pdf file: 'aspect.pdf', trim: 37, margin: 120
    if ARGV.include?("png")
      save_png rotate: :counterclockwise, count_format: '%02d', dir: '_output/aspects', prefix: 'element'
    else
      save_pdf file: 'element.pdf', margin: 120
    end
  end
end

if ARGV.size == 0 or ARGV.include?("actions") or ARGV.include?("aspects")
  # Action Cards
  Squib::Deck.new(cards: 49, layout: 'yml/minicard.yml', width: 825, height: 600) do
    background color: 'white'
    deck = csv file: 'csv/actions.csv'
  
    rect layout: 'MiniCard', fill_color: :black
    rect layout: 'MiniBleed', fill_color: makeRadial("beige", "CINNAMON")
    rect layout: 'Text', fill_color: :beige, stroke_color: :black

    titleLayout = deck['Title'].map{|x|
      case x
      when "Preachin'", "Prophet", "Crusade"
        :RTitle
      else
        :CTitle
      end
    }
    rect layout: titleLayout#, fill_color: :black
    text str: deck['Title'], layout: titleLayout#, color: :white
    text str: deck['Text'], layout: :Text
    leftsymbol = deck['Aspect'].map{|x|
      case x
      when "Any"
        "Preachin'2"
      when "Negative"
        "Crusade2"
      when "None"
        "Empty"
      else
        x + "Prophet"
      end
    }
    svg file: leftsymbol.map{|x| svgPath(x)}, layout: :AspectColor
    rect layout: 'Type'#, fill_color: :black
    text str: deck['Rank'].map{|x| 'Action ' + x}, layout: :Type#, color: :white
  
    if png
      save_png rotate: :counterclockwise, count_format: '%02d', dir: '_output/aspects', prefix: 'action'
    else
      save_pdf file: 'action.pdf', margin: 120
    end
  end
end

if ARGV.size == 0 or ARGV.include?("headers")
  # Header Cards
  Squib::Deck.new(cards: 49, layout: 'yml/pokercard.yml') do
    background color: 'white'
    deck = csv file: 'csv/headers.csv'
    #deck['Color']
    rect layout: 'PokerCard', fill_color: :black
    rect layout: 'PokerBleed', fill_color: :white
    svg file: deck['Title'].map{|x| svgPath(x+"Header")} * 7, layout: :Header
    rect layout: :Title, fill_color: :white
    text str: deck['Title'] * 7, layout: :Title
    rect layout: :Center
    #png file: deck['Color'].map{|x| "png/" + x + "Back.png"}, crop_x: 100, crop_y: 250, crop_width: 625, crop_height: 600, alpha: 0.5, x: 100, y: 400
    png file: deck['Color'].map{|x| "png/" + x + "Symbol.png"}, x: 112.5, y: 400, width: 600, height: 600, alpha: 0.2
    text str: deck['Label'].map{|x| "\n"+x}, layout: :Center, valign: :top
  
    if png
      save_png prefix: deck['Color'].zip(deck['Title']).map{|x| x[0] + x[1]},count_format: '', dir: '_output/headers'
    else
      save_pdf file: 'header.pdf', trim: 37, margin: 120
    end
  end
end

if ARGV.size == 0 or ARGV.include?("box")
  # Header Cards
  deck = ["Top", "Bottom"]
  Squib::Deck.new(cards: 2, width: 3300, height: 4350) do
    svg file: deck.map{|x| "box/Box\ " + x + ".svg"}, x: 0, y: 0
    if png
      save_png prefix: deck, count_format: '', dir: '_output/box'
    end
  end
end
