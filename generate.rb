require 'csv'

def makeNames(csvFile, restriction)
  names = CSV.read(csvFile).drop(1)
  used = {}
  ->n {
    rets = []
    for i in 0..(n - 1)
      r = rand(names.length)
      name = names[r][1]
      while(used.key?(name) or name.length > restriction)
        r = rand(names.length)
        name = names[r][1]
      end
      used[name] = 0
      rets[i] = name
    end
    rets
  }
end

def writeNames(file, n, namer)
  open('names/' + file, 'w') { |f|
    f.puts 'Names'
    f.puts namer.(n).join("\n")
  }
end

def makeEmpires(csvFile)
  names = CSV.read(csvFile).drop(1)
  used = {}
  ->n {
    rets = []
    for i in 0..(n - 1)
      r = rand(names.length)
      name = names[r][0]
      while(used.key?(name))
        r = rand(names.length)
        name = names[r][0]
      end
      used[name] = 0
      rets[i] = name
    end
    rets
  }
end

def writeEmpires(file, n, empirer)
  open('names/' + file, 'w') { |f|
    f.puts 'Empires'
    f.puts empirer.(n).join("\n")
  }
end

namer = makeNames('names/baby-names.csv', 6)
empty = ARGV.size == 0
onlyNames = ARGV.size == 1 and ARGV.include?("names")
onlyEmpires = ARGV.size == 1 and ARGV.include?("empires")
# 3 followers - 21 (x3) = 63
if empty or onlyNames or (ARGV.include?("3follower") and ARGV.include?("names"))
  writeNames('3followerNames.csv', 63, namer)
end

# 2 followers - 28 (x2) = 56
if empty or onlyNames or (ARGV.include?("2follower") and ARGV.include?("names"))
  writeNames('2followerNames.csv', 56, namer)
end

# 1 followers - 35 (x1) = 35
if empty or onlyNames or (ARGV.include?("1follower") and ARGV.include?("names"))
  writeNames('1followerNames.csv', 35, namer)
end
# Total = 63 + 56 + 35 = 154

empirer = makeEmpires('names/empires.csv')
# 3 followers - 21 (x3) = 63
if empty or onlyEmpires or (ARGV.include?("3follower") and ARGV.include?("empires"))
  writeEmpires('3followerEmpires.csv', 7, empirer)
end

# 2 followers - 28 (x2) = 56
if empty or onlyEmpires or (ARGV.include?("2follower") and ARGV.include?("empires"))
  writeEmpires('2followerEmpires.csv', 14, empirer)
end

# 1 followers - 35 (x1) = 35
if empty or onlyEmpires or (ARGV.include?("1follower") and ARGV.include?("empires"))
  writeEmpires('1followerEmpires.csv', 7, empirer)
end
# Total = 63 + 56 + 35 = 154
